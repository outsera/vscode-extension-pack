# vscode-extension-pack
## Texo IT Extension Pack for Visual Studio Code
This extension pack includes tools for working with the current tech stack.

## Extensions Included
* [alefragnani.project-manager](https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager)  
   Easily switch between projects
* [christian-kohler.path-intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)  
   Visual Studio Code plugin that autocompletes filenames
* [dbaeumer.vscode-eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)  
   Integrates ESLint into VS Code.
* [DotJoshJohnson.xml](https://marketplace.visualstudio.com/items?itemName=DotJoshJohnson.xml)  
   XML Formatting, XQuery, and XPath Tools for Visual Studio Code
* [EditorConfig.EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)  
   EditorConfig Support for Visual Studio Code
* [eg2.tslint](https://marketplace.visualstudio.com/items?itemName=eg2.tslint)  
   TSLint for Visual Studio Code
* [eriklynd.json-tools](https://marketplace.visualstudio.com/items?itemName=eriklynd.json-tools)  
   Tools for manipulating JSON
* [hnw.vscode-auto-open-markdown-preview](https://marketplace.visualstudio.com/items?itemName=hnw.vscode-auto-open-markdown-preview)  
   Open Markdown preview automatically when opening a Markdown file
* [johnpapa.Angular2](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2)  
   Angular with TypeScript snippets.
* [marcostazi.VS-code-vagrantfile](https://marketplace.visualstudio.com/items?itemName=marcostazi.VS-code-vagrantfile)  
   Provides syntax highlighting support for Vagrantfile, useful if you use Vagrant
* [ms-vscode.Theme-MarkdownKit](https://marketplace.visualstudio.com/items?itemName=ms-vscode.Theme-MarkdownKit)  
   Theme Kit for VS Code optimized for Markdown. Based on the TextMate themes.
Install
* [ms-vscode.Theme-MaterialKit](https://marketplace.visualstudio.com/items?itemName=ms-vscode.Theme-MaterialKit)  
   Material themes for VS Code. Based on the TextMate themes.
* [P-de-Jong.vscode-html-scss](https://marketplace.visualstudio.com/items?itemName=P-de-Jong.vscode-html-scss)  
   SCSS support for HTML documents
* [PeterJausovec.vscode-docker](https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker)  
   Adds syntax highlighting, snippets, commands, hover tips, and linting for Dockerfile and docker-compose files.
* [PKief.material-icon-theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)  
   Material Design Icons for Visual Studio Code
* [rbbit.typescript-hero](https://marketplace.visualstudio.com/items?itemName=rbbit.typescript-hero)  
   Additional tooling for the typescript language
* [robertohuertasm.vscode-icons](https://marketplace.visualstudio.com/items?itemName=robertohuertasm.vscode-icons)  
   Icons for Visual Studio Code
* [zhuangtongfa.Material-theme](https://marketplace.visualstudio.com/items?itemName=zhuangtongfa.Material-theme)  
   Atom's iconic One Dark theme for Visual Studio Code
* [Zignd.html-css-class-completion](https://marketplace.visualstudio.com/items?itemName=Zignd.html-css-class-completion)  
   Provides CSS class name completion for the HTML class attribute based on the CSS files in your workspace. Also supports React's className attribute

## Getting started

You can install it as a regular extension, if help is needed just access [this](https://code.visualstudio.com/docs/editor/extension-GALLERY).

## License
[MIT](https://bitbucket.org/texoit/vscode-extension-pack/src/master/LICENSE.md) &copy; Texo IT