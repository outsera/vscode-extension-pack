# Change Log
All notable changes to the extension pack will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [1.0.0] - 2017-09-03
### Added
- The initial structure and extension dependencies

## [1.0.1] - 2017-09-04
### Removed
- Extension "alphabotsec.vscode-eclipse-keybindings", not everyone wants it as default

